# Invodo Ivp generator

[Yeoman](http://yeoman.io) generator that scaffolds a [Shoppable Video Player](http://invodo.com/shoppable-video) ( formerly Ivp ) experience.

![](http://ixd.invodo.com/images/ivp-screenshot-v2.0.png)

## Features

* CSS Autoprefixing
* Built-in preview server with LiveReload
* Automagically compile ES6 (with Babel) & Sass
* Automagically lint your scripts
* Awesome Image Optimization (via OptiPNG, pngquant, jpegtran and gifsicle)
* Mocha or Jasmine Unit Testing with PhantomJS

For more information on what `generator-ivp` can do for you, take a look at the Grunt tasks used in our `package.json`.


## Getting Started

```bash
$ git clone git@bitbucket.org:invodo-interactive/generator-ivp.git
$ cd generator-ivp
$ npm link
$ yo ivp
$ npm install && bower install
```

- Run `grunt` for building and `grunt serve` for preview
- Run `grunt serve:dist` to preview production ready assets

## Docs

We have [recipes](docs/recipes) for integrating other popular technologies like Compass.


## Options

* `--skip-install`

  Skips the automatic execution of `bower` and `npm` after scaffolding has finished.

* `--test-framework=<framework>`

  Defaults to `jasmine`. Can be switched for another supported testing framework like `mocha`.

* `--babel`

  Add support for [Babel](http://babeljs.io/).
