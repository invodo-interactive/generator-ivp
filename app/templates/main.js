// jshint devel:true

// Invodo Init must be called only once per page
Invodo.init({pageName: '<%= appname %> Test Page', pageType: 'product', debugLogger: 'true'});

(function(){

  // Get the experience element
  var el = document.getElementById('svp-experience');
  // Create Ivp args
  var args = {
    refId: 'NJ8MQ6FX',
    dataPath: 'scripts/testing-data.json'
  };
  // Create Ivp instance
  var ivp = Invodo.Ixd.Ivp(el, args);

})();
