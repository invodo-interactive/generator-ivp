'use strict';
var join = require('path').join;
var yeoman = require('yeoman-generator');
var chalk = require('chalk');

module.exports = yeoman.generators.Base.extend({
  constructor: function () {
    yeoman.generators.Base.apply(this, arguments);

    // setup the test-framework property, Gruntfile template will need this
    this.option('test-framework', {
      desc: 'Test framework to be invoked',
      type: String,
      defaults: 'jasmine'
    });
    this.testFramework = this.options['test-framework'];

    this.option('babel', {
      desc: 'Use Babel',
      type: Boolean,
      defaults: false
    });

    this.babel = this.options.babel;

    //this.babel = true;

    this.pkg = require('../package.json');
  },

  askFor: function () {
    var done = this.async();

    // welcome message
    if (!this.options['skip-welcome-message']) {
      this.log(require('yosay')());
      this.log(chalk.magenta(
        'You\'re making a Shoppable Video project? ' +
        'Well that\'s pretty cool.'
      ));
    }

    // var prompts = [{
    //   type: 'checkbox',
    //   name: 'features',
    //   message: 'What more would you like?',
    //   choices: [{
    //     name: 'Bootstrap',
    //     value: 'includeBootstrap',
    //     checked: true
    //   },{
    //     name: 'Sass',
    //     value: 'includeSass',
    //     checked: false
    //   },{
    //     name: 'Modernizr',
    //     value: 'includeModernizr',
    //     checked: false
    //   }]
    // }];

    var prompts = [];

    this.prompt(prompts, function (answers) {
      // var features = answers.features;
      //
      // function hasFeature(feat) {
      //   return features && features.indexOf(feat) !== -1;
      // }
      //
      // this.includeSass = hasFeature('includeSass');
      // this.includeBootstrap = hasFeature('includeBootstrap');
      // this.includeModernizr = hasFeature('includeModernizr');



      done();
    }.bind(this));
  },

  gruntfile: function () {
    this.template('Gruntfile.js');
  },

  packageJSON: function () {
    this.template('_package.json', 'package.json');
  },

  git: function () {
    this.template('gitignore', '.gitignore');
    this.copy('gitattributes', '.gitattributes');
  },

  bower: function () {
    var bower = {
      name: this._.slugify(this.appname),
      private: true,
      dependencies: {
        "ivp" : "https://bitbucket.org/invodo-interactive/ivp-public.git"
      }
    };

    this.copy('bowerrc', '.bowerrc');
    this.write('bower.json', JSON.stringify(bower, null, 2));
  },

  jshint: function () {
    this.copy('jshintrc', '.jshintrc');
  },

  editorConfig: function () {
    this.copy('editorconfig', '.editorconfig');
  },

  mainStylesheet: function () {
    this.template('main.scss', 'app/styles/main.scss');
  },

  writeIndex: function () {
    this.indexFile = this.engine(
      this.readFileAsString(join(this.sourceRoot(), 'index.html')),
      this
    );

    this.indexFile = this.appendFiles({
      html: this.indexFile,
      fileType: 'js',
      optimizedPath: 'scripts/main.js',
      sourceFileList: ['scripts/main.js'],
      searchPath: ['app', '.tmp']
    });
  },

  writeProduction: function () {
    this.prodFile = this.engine(
      this.readFileAsString(join(this.sourceRoot(), 'prod.html')),
      this
    );

    this.prodFile = this.appendFiles({
      html: this.prodFile,
      fileType: 'js',
      optimizedPath: 'scripts/main.js',
      sourceFileList: ['scripts/main.js'],
      searchPath: ['app', '.tmp']
    });
  },

  app: function () {
    this.directory('app');
    this.mkdir('app/scripts');
    this.mkdir('app/styles');
    this.mkdir('app/images');
    this.write('app/index.html', this.indexFile);
    this.write('app/prod.html', this.prodFile);

    this.copy('main.js', 'app/scripts/main.js');
    this.copy('testing-data.json', 'app/scripts/testing-data.json');
  },

  install: function () {
    this.on('end', function () {
      this.invoke(this.options['test-framework'], {
        options: {
          'skip-message': this.options['skip-install-message'],
          'skip-install': this.options['skip-install'],
          'babel': this.options.babel
        }
      });

      if (!this.options['skip-install']) {
        this.installDependencies({
          skipMessage: this.options['skip-install-message'],
          skipInstall: this.options['skip-install']
        });
      }
    });
  }
});
